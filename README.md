Role Name
=========

The goal is to deploy a Mac using mostly Homebrew.  Bonus goal would be to do post install setup like adding Github and Amazon creds.

Requirements
------------

This playbook is run from a local connection via:

```
ansible-playbook -i hosts site.yml --ask-sudo-pass
```


Homebrew installed

```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Ansible installed via Brew

```
brew install ansible --HEAD
```

Github Auth'd

```
cat ~/.ssh/id_rsa.pub & add that to GitHub
```

Github API token set

_http://stackoverflow.com/questions/20130681/setting-github-api-token-for-homebrew#20130816_

After the script has finished then run the .dotfiles setup script to complete the setup.

```
~/.dotfiles/script/bootstrap
```

Role Variables
--------------

mac_username: local user on the Mac that is logged in and running this playbook.


Dependencies
------------



Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

GNU General Public License v2.0

Author Information
------------------

Chris Livermore
[@e30chris](https://twitter.com/e30chris)
[Sandors Systems Scribbles](http://sandorsscribbl.es/)
